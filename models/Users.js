const mongoose = require('mongoose')

const userSchema = new mongoose.Schema({

	firstName: {
		type: String,
		required: [true, 'Enter First Name.']
	},

	lastName: {
		type: String,
		required: [true, 'Enter Last Name.']
	},

	email: {
		type: String,
		required: [true, 'Enter Email']
	},

	password: {
		type: String,
		required: [true, 'Enter Password']
	},

	isAdmin: {
		type: Boolean,
		default: false
	},

	mobileNo: {
		type: String,
		required: [true, 'Enter Mobile Number']
	},

	enrollments: [{
		courseId: {
			type: String,
			required: [true, 'Enter Course ID']
		},

		enrolledOn: {
			type: Date,
			default: new Date()
		},

		status: {
			type: String,
			default: "enrolled"
		}
	}]
})

module.exports = mongoose.model('User', userSchema)
